# Script para treinar selenium no site https://try.discourse.org/

from browser import *
from selenium.webdriver.common.by import By

class Testando (Browser):
    def __init__(self):
        super().__init__()
        self.driver.get("https://try.discourse.org/")

# Buscar nome dos tópicos da página
    def search_topic_name(self):
        print('Buscando nome dos tópicos...')
        self.scrolling_to_final()
        topics_name = []
        #buffer_frames = self.driver.find_elements("xpath", "//div//tr/td/span")
        buffer_frames = self.driver.find_elements(By.CSS_SELECTOR, ".link-top-line .title")

        for i in range(len(buffer_frames)):
            try:
                #link = buffer_frames[i].find_element("xpath", "./a").text
                link = buffer_frames[i].text
                topics_name.append(link)
            except:
                print("Vou ignorar. Falta de elemento dentro da div")
        return topics_name

# Buscar nome das categorias dos tópicos da página
    def category_name(self):
        print('Buscando nome dos tópicos...')
        self.scrolling_to_final()
        nome_categoria = []
        buffer_frames = self.driver.find_elements(By.CSS_SELECTOR, ".category-name")

        for i in range(len(buffer_frames)):
            try:
                link = buffer_frames[i].text
                nome_categoria.append(link)
            except:
                print('Vou ignorar. Falta de elemento dentro da div')
        return nome_categoria

# Buscar nome dos tópicos e seus respectivos links
    def topic_name_and_link(self):
        print('Buscando nome dos tópicos e seus links...')
        self.scrolling_to_final()
        topic_name = {}
        topic_name_link = []
        buffer_frames = self.driver.find_elements(By.CSS_SELECTOR, ".link-top-line .title")

        for i in range(len(buffer_frames)):
            try:
                topic_name['Topico'] = buffer_frames[i].text
                topic_name['Link'] = buffer_frames[i].get_attribute("href")
                topic_name_link.append(topic_name.copy())
            except:
                print('Vou ignorar. Falta de elementro dentro da div')
        return topic_name_link

# Verificar quantidade total de tópicos da categoria 'X'
    def check_quantity_topic_x_category(self, category):
        self.scrolling_to_final()
        buffer_category = self.driver.find_elements(By.CSS_SELECTOR, ".category-name")
        total = 0
        for i in range(len(buffer_category)):

            try:
                if category in buffer_category[i].text:
                    total += 1
            except:
                None
        return f'Tem {total} tópicos da categoria {category}'

# Criar função para rolar a página até o final
    def scrolling_to_final(self):
        import time

        last_height = self.driver.execute_script("return document.body.scrollHeight")
        
        existe = ''
        try:
            existe = self.driver.find_element(By.XPATH, "//*[text()='There are no more latest topics.']").text
        except:
            None

        while existe != 'There are no more latest topics.':
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)
            new_height = self.driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height

# Verificar quantidade total de categorias
    def check_total_categories(self):
        dados = self.category_name()

        lista = []

        for i in range(len(dados)):
            if dados[i] not in lista:
                lista.append(dados[i])
        return f'Total de categorias no fórum: {len(lista)}'



# Verificar se categoria 'X' existe

# Buscar nome do(s) tópico(s) que existem da categoria 'X'

# Verificar quantos tópicos estão fechados

# Verificar quantos tópicos estão abertos

# Buscar quantidade de views total da página (td.views)

# Buscar quantidade de views de cada categoria

# Verificar qual tópico com maior quantidade de views

# Verificar qual tópico com menor quantidade de views

# Buscar quantidade de respostas total da página ()

# Buscar quantidade de respostas de cada categoria

# Verificar qual tópico com maior quantidade de respostas

# Verificar qual tópico com menor quantidade de respostas